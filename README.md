# QA Engineer - Technical demonstration
This repository demonstrates RESTful API tests with right level of assertions using an appropriate test framework which is capable of integrating with any modern CI/CD pipeline.

The RESTful APIs are exposed by a service whose docker image is available in dockerhub at sudharsansun/sen-heal-qa-server.

## Software pre-requistes
- Latest and stable docker Desktop, Community Edition (CE).

_Please note that this test is developed and tested in macbook_

## Run Functional test
To run functional tests, issue the following commands.
```
> # Run the backend service
>
> docker pull sudharsansun/sen-heal-qa-server:v1
> docker run -p 5000:5000 -t sudharsansun/sen-heal-qa-server:v1
>
>
> # Run the tests
>
> docker build -t api-tests .
> docker run --network="host" api-tests
```
## Run Performance test
These tests only verifies HTTP response code when APIs are called concurrently.

To run performance tests, issue the following command.
```
> # Run the tests
> 
> # Ensure backend service is up and running.
> 
> # Ensure to be in the repository home folder.
> 
> docker pull loadimpact/k6
> 
> docker run -e K6_DURATION=10s -e K6_RPS=5 --network="host"   -i loadimpact/k6 run - < ./performane-test/index.js
```

## Development
The test uses `mocha.js` test framework and `chai.js` asertion library. The tests run in `docker` containers.

For development purposes it is required to have `node.JS`, with a minimum v12.16.1.

To install the application, issue the command
```
> npm install
```

Eslint code linters with `airbnb` specifications are used. Ensure that code always lints by issuing the command
```
> npm run lint
```

To run the test, issu the command
```
> npm run start
```
