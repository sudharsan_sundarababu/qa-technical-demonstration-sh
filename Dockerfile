FROM node:12.16.1 as base

ENV TEST_DIR = /opt/api/tests

FROM base

WORKDIR $TEST_DIR

COPY . .

RUN npm install

CMD ["npm", "start"]
