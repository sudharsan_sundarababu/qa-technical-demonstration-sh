const { describe, it, beforeEach } = require('mocha');
const { assert } = require('chai');
const util = require('util');

const { getProducts, clearProducts, addProduct } = require('../lib/api');
const { createProducts } = require('../lib/helper');

describe('Sensyne test API', () => {
  beforeEach(async () => {
    await clearProducts();
  });

  describe('GET /products', async () => {
    it('should return empty list when no products are added', async () => {
      // Seed test data
      await clearProducts();

      // Call endpoint
      const getProductsResponse = await getProducts();

      // Assertions
      assert.equal(
        getProductsResponse.data.length,
        0,
        `Expected to see no products in the response, but received ${getProductsResponse.data.length} products`
      );
      assert.equal(
        getProductsResponse.status,
        404,
        `Expected HTTP Response code 404 while calling the endpoint GET /products when no products are stored, but received ${getProductsResponse.status}.`
      );
    });

    it('Should return product information', async () => {
      // Seed test data
      const product = {
        name: 'Hand Sanitiser',
        price: 10.2
      };
      await addProduct(product);

      // Call endpoint
      const getProductsResponse = await getProducts();
      const responseProduct = getProductsResponse.data[0];

      // Assertions
      assert.equal(
        200,
        getProductsResponse.status,
        `Expected HTTP Response code to be 200, but received ${getProductsResponse.status}`
      );
      assert.equal(
        getProductsResponse.data.length,
        1,
        `Expected to see 1 product in the response, but received ${getProductsResponse.data.length}`
      );
      assert.typeOf(getProductsResponse.data, 'array');
      assert.notTypeOf(responseProduct.name, 'undefined', `Expected GET /products response ${util.inspect(responseProduct, true, null, true)} to have an attribute "name", but not found.`);
      assert.notTypeOf(responseProduct.product_code, 'undefined', `Expected GET /products response ${util.inspect(responseProduct, true, null, true)} to have an attribute "product_code", but not found.`);
      assert.notTypeOf(responseProduct.product_price, 'undefined', `Expected GET /products response ${util.inspect(responseProduct, true, null, true)} to have an attribute "product_price", but not found.`);
    });

    it('Should return product information of all the added products ', async () => {
      // Seed test data
      const expectedProductsToAdd = await createProducts();

      // Call endpoint
      const getProductsResponse = await getProducts();

      // Assertions
      assert.equal(
        200,
        getProductsResponse.status,
        `Expected GET /products Response code to be 200, but received ${getProductsResponse.status}`
      );
      assert.equal(
        getProductsResponse.data.length,
        expectedProductsToAdd.length,
        `Expected to see ${expectedProductsToAdd.length} number of products in the response, but received ${getProductsResponse.data.length}`
      );
    });
  });

  describe('GET /product by product code', async () => {
    beforeEach(async () => {
      await clearProducts();
    });

    it('Should return product information identified by product code', async () => {
      // Seed test data
      await createProducts();

      // Call endpoint and assertions
      const getProductsResponse = await getProducts();
      getProductsResponse.data.forEach(async (product) => {
        const productByCode = await getProducts(product.id);
        assert.equal(
          200,
          productByCode.status,
          `Expected HTTP Response code to be 200OK, but received ${productByCode.status}`
        );
      });
    });

    it('Should return an error if product code is invalid', async () => {
      try {
        // Call endpoint
        await getProducts(-99); // Non-existing product code
        assert.fail('Expected an error while retreiving product details of an invalid code.');
      } catch (error) {
        // Assertions
        assert.equal(
          404,
          error.response.data.status,
          `Expected HTTP Response code to be 404, but received ${error.response.data.status}`
        );
      }
    });
  });

});
