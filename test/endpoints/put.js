const { describe, it, beforeEach } = require('mocha');
const { assert } = require('chai');

const {
  getProducts, clearProducts, updateProduct, addProduct
} = require('../lib/api');

describe('Sensyne test API', () => {
  beforeEach(async () => {
    await clearProducts();
  });

  describe('PUT /product by product code', async () => {
    it('Should update product information of a particular product', async () => {
      // Seed test data
      const products = [{
        name: 'Hand Sanitiser',
        price: 10.2
      },
      {
        name: 'Frozen Potato',
        price: 1.75
      }];
      products.forEach(async (product) => {
        await addProduct(product);
      });

      // Call endpoint
      const updateProductReponse = await updateProduct(1, {
        name: 'Hand Towels',
        price: 1.43,
      });

      // Assertions

      const getProductsResponse = await getProducts(1);
      const responseProduct = getProductsResponse.data;
      assert.equal(
        updateProductReponse.status,
        200,
        `Expected PUT /product Response code to be 200, but received ${getProductsResponse.status}`,
      );
      assert.equal(
        responseProduct.name,
        'Hand Towels',
        'Expected Product name be updated to "Hand Towels", but original name is retained'
      );
      assert.equal(
        responseProduct.price,
        '1.43',
        'Expected Product name be updated to "1.0", but original name is retained'
      );
    });

    it('Should return an error if product code is invalid', async () => {
      try {
        // Call endpoint
        await updateProduct(99, {
          name: 'Hand Towels',
          price: 1.43,
        }); // Non-existing product code
        assert.fail('Expected an error while updating a product, but service allows this operation.');
      } catch (error) {
        // Assertions
        assert.equal(
          404,
          error.response.data.status,
          `Expected PUT /product Response code to be 404, but received ${error.response.data.status}`,
        );
      }
    });

    it.skip('Should return an error if updated data is invalid', async () => { // Skipping this test due to lack of error description in the swagger file
      // Seed test data
      const products = [{
        name: 'Hand Sanitiser',
        price: 10.2
      },
      {
        name: 'Frozen Potato',
        price: 1.75
      }];
      products.forEach(async (product) => {
        await addProduct(product);
      });

      // Call endpoint and assert
      try {
        await updateProduct(1, {
          name: 99,
          price: 1.43,
        });
        assert.fail('Expected an error while retreiving product details of an invalid code.');
      } catch (error) {
        assert.equal(
          404, // Undefined error code in swagger
          error.response.data.status,
          `Expected PUT /product Response code to be 404, but received ${error.response.data.status}`,
        );
      }
    });
  });
});
