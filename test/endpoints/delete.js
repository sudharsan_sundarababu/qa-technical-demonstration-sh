const { describe, it, beforeEach } = require('mocha');
const { assert } = require('chai');

const { clearProducts } = require('../lib/api');
const { createProducts } = require('../lib/helper');

describe('Sensyne test API', async () => {
  beforeEach(async () => {
    await clearProducts();

  });

  describe('DELETE /product by code', async () => {
    it('Should remove a particular product identified by product code', async () => {
      // Seed test data
      await createProducts();

      // Call endpoint and assertions
      const clearProductResponse = await clearProducts(1);
      assert.equal(
        200,
        clearProductResponse.status,
        `Expected DELETE /products Response code to be 200, but received ${clearProductResponse.status}`
      );

    });

    it('Should return an error if product code is invalid', async () => {
      try {
        // Call endpoint
        await clearProducts(-99); // Non-existing product code
        assert.fail('Expected an error while deleting a non existing product, but service allows this operation.');
      } catch (error) {
        // Assertions
        assert.equal(
          404,
          error.response.data.status,
          `Expected DELETE /products Response code to be 404, but received ${error.response.data.status}`
        );
      }
    });
  });

});
