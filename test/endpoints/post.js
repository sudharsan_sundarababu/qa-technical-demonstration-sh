const { describe, it, beforeEach } = require('mocha');
const randomstring = require('randomstring');
const { assert } = require('chai');

const { clearProducts, addProduct } = require('../lib/api');

describe('Sensyne test API', () => {
  beforeEach(async () => {
    await clearProducts();
  });

  describe('POST /product', async () => {
    it('Should add a new product to the list', async () => {
      // Call endpoint
      const product = {
        name: 'Hand Sanitiser',
        price: 10.2
      };
      const createProductResponse = await addProduct(product);

      // Assertions
      assert.equal(
        200,
        createProductResponse.status,
        `Expected POST /product Response code to be 200, but received ${createProductResponse.status}`,
      );
    });

    it('Should allow to set product name with a any valid swagger string format', async () => {
      // Call endpoint
      const product = {
        name: `${randomstring.generate({
          length: 7,
          charset: 'äöü構わないäöü構わないअआघ' // utf-8 charset with multiple language support
        })}`,
        price: 10.2
      };
      const createProductResponse = await addProduct(product);

      // Assertions
      assert.equal(
        200,
        createProductResponse.status,
        `Expected POST /product Response code to be 200, but received ${createProductResponse.status}`,
      );
    });

    it('Should allow to set product price with a any valid swagger number format', async () => {
      // Call endpoint
      const product = {
        name: `${randomstring.generate({
          length: 7,
          charset: 'äöü構わないäöü構わないअआघ'
        })}`,
        price: -10.2
      };
      const createProductResponse = await addProduct(product);

      // Assertions
      assert.equal(
        200,
        createProductResponse.status,
        `Expected POST /product Response code to be 200, but received ${createProductResponse.status}`,
      );
    });

    it.skip('Should return an error if product information is invalid', async () => { // Skipping this test due to lack of error description in the swagger file
      // Call endpoint
      const product = {
        price: -10.2, // missing the mandatory attribute "name"
      };
      const createProductResponse = await addProduct(product);

      // Assertions
      assert.equal(
        400, // Undefined error code in swagger
        createProductResponse.status,
        `Expected POST /product Response code to be 400, but received ${createProductResponse.status}`,
      );
    });
  });
});
