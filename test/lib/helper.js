const { productNames } = require('./config');
const { addProduct } = require('./api');

const sleep = async () => {
  const delay = Math.floor(
    (Math.random() * 1000000) % 3500
  );
  setTimeout(async () => Promise.resolve(), delay);
};

const createProducts = async () => {

  const productsToAdd = [];
  let count = Math.floor(
    (Math.random() * 100) % productNames.length
  );
  if (count === 0) count = 1;

  for (let i = 1; i <= count; i += 1) {
    productsToAdd.push({
      name: productNames[i],
      price: 1.51
    });
  }

  const addProductRequests = [];
  productsToAdd.forEach(async (product) => {
    addProductRequests.push(addProduct({
      name: product.name,
      price: product.price,
    }));
  });

  const expectedProducts = productsToAdd.map((product) => {
    const container = {};
    container.name = product.name;
    container.price = product.price.toString();
    return container;
  });

  await Promise.all(addProductRequests);
  return expectedProducts;
};

module.exports = {
  sleep,
  createProducts
};
