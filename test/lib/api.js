const axios = require('axios');
const { url } = require('./config');

const get = async (code) => {
  let path = 'v1//products';
  if (typeof code !== 'undefined') {
    path = `v1//product/${code}`;
  }
  const endpoint = `${url}${path}`;

  return axios.get(endpoint);
};

const post = (product) => {
  const {
    name,
    price,
  } = product;
  const path = 'v1/product';
  const endpoint = `${url}${path}`;

  return axios({
    method: 'post',
    url: endpoint,
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({
      name,
      price,
    })
  });
};

const del = async (id) => {
  const path = 'v1/product';
  const endpoint = `${url}${path}`;

  if (typeof id === 'undefined') {
    // Read the product list and delete all the ids
    const products = await get();
    const deleteProductRequests = [];
    products.data.forEach((element) => {
      const deleteUrl = `${endpoint}/${element.id}`;
      deleteProductRequests.push(axios.delete(deleteUrl));
    });
    return Promise.all(deleteProductRequests);
  }
  // Delete a paricular product id
  const deleteUrl = `${endpoint}/${id}`;
  return axios.delete(deleteUrl);
};

const put = (code, product) => {
  const {
    name,
    price,
  } = product;
  const path = 'v1/product';
  const endpoint = `${url}${path}/${code}`;

  return axios({
    method: 'put',
    url: endpoint,
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({
      name,
      price,
    })
  });
};


module.exports = {
  getProducts: get,
  addProduct: post,
  clearProducts: del,
  updateProduct: put,
};
