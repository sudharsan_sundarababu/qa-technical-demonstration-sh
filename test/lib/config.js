const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 5000;

const url = `http://${host}:${port}/`;

const productNames = [
  'Hand Sanitizer',
  'Jam',
  'Hand Towels',
  'Frozen Potatoes',
  'Jam',
  'Bread',
  'Milk',
  'Yogurt',
  'Frozen Pizza'
];


module.exports = {
  url,
  productNames,
};
