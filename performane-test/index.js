/* eslint-disable import/no-unresolved */
import http from 'k6/http';
import { check } from 'k6';

export default function () {
  const getProducts = {
    method: 'GET',
    url: 'http://localhost:5000/v1/products',
  };
  const getProductById = {
    method: 'GET',
    url: 'http://localhost:5000/v1/product/1',
  };
  const postNewProduct = {
    method: 'POST',
    url: 'http://localhost:5000/v1/product',
    body: JSON.stringify({
      name: 'Frozen Peas',
      price: 1.3
    }),
    params: {
      headers: { 'Content-Type': 'application/json' },
    },
  };
  const putUpdateProduct = {
    method: 'PUT',
    url: 'http://localhost:5000/v1/product',
    body: JSON.stringify({
      name: 'Frozen Potatoes',
      price: 1.3
    }),
    params: {
      headers: { 'Content-Type': 'application/json' },
    },
  };

  const responses = http.batch([
    getProducts,
    getProductById,
    postNewProduct,
    putUpdateProduct]);

  check(responses[0], { 'Is GET /products Response status is 200 ?': (res) => res.status === 200 });
  check(responses[1], { 'Is GET /product/1 Response status is 200 ?': (res) => res.status === 200 });
  check(responses[2], { 'Is POST /product Response status is 200 ?': (res) => res.status === 200 });
  check(responses[3], { 'Is PUT /product Response status is 200 ?': (res) => res.status === 200 });
}
